#encoding: utf-8
"""
Utilities classes.
"""

import time

import config

class Log(object):
    """
    Simple utility class to log easily.
    """
    @classmethod
    def log(cls, message):
        """
        Logging message with timestamp.
        """
        actual_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        with open(config.LOG_FILE, 'a') as file_handle:
            try:
                file_handle.write("%s: %s\n" % (actual_time, message.encode("utf-8")))
            except UnicodeDecodeError:
                file_handle.write("%s: %s\n" % (actual_time, message))

    @classmethod
    def debug(cls, message):
        """
        Manage DEBUG level of logging.
        """
        if config.LOG_LEVEL >= config.DEBUG:
            cls.log("%s: %s" % ("DEBUG", message))

    @classmethod
    def warning(cls, message):
        """
        Manage WARNING level of logging.
        """
        if config.LOG_LEVEL >= config.WARNING:
            cls.log("%s: %s" % ("WARNING", message))

    @classmethod
    def info(cls, message):
        """
        Manage INFO level of logging.
        """
        if config.LOG_LEVEL >= config.INFO:
            cls.log("%s: %s" % ("INFO", message))

    @classmethod
    def error(cls, message):
        """
        Manage ERROR level of logging.
        """
        if config.LOG_LEVEL >= config.ERROR:
            cls.log("%s: %s" % ("ERROR", message))

