# encoding: utf-8
"""
Bot Wantzel from La Quadrature du Net.

License : AGPLv3
Doc     : https://wiki.laquadrature.net/Wantzel
"""

import re

from irc import IrcClientFactory
from twisted.internet import reactor, task, ssl

import config
from logs import Log

# bot modules
import helpers
from admin import Admin
from monitor import Monitoring
from rp import Rp

class Wantzel(object):
    """
    Wantzel bot.
    """
    def __init__(self):
        """
        Initialization of all utility objects and bot over IRC.
        """
        self.admin = Admin()
        self.monitor = Monitoring()
        self.rp = Rp()
        # Connection to IRC
        self.irc = IrcClientFactory(config)
        self.irc.set_callbacks = self.set_callbacks
        reactor.connectSSL(config.server, config.port, self.irc, ssl.ClientContextFactory())
        self.test_timer = ""
        self.topic = ""
        # Loop call
        self.loop_started = False
        self.loop = task.LoopingCall(self.timer)

    def timer(self):
        """
        This method launches function regularly (see config.timer).
        """
        Log.debug("Timer called")
        # Testing timer ?
        if self.test_timer!="":
            self.send_message(self.test_timer, "Timer fonctionnel!")        
            self.test_timer = ""
        # Tweeting Press reviews f necessary
        self.rp.tweet()
        # Update topic based on number of articles waiting in queue if necessary
        topic = self.rp.count_articles()
        if topic != self.topic:
            self.irc.client.topic(config.RP_CHANNEL, topic)
        # Tell on channel if a wiki was modified since last time
        self.send_message(config.MONITOR_CHANNEL, self.monitor.update())
        # Cleaning points of mastering rp
        self.rp.clean_master_rp()

    def set_callbacks(self):
        """
        This method set the methods to call for each callback received from IRC.
        """
        # When receiving a message
        self.irc.client.privmsg = self.on_privmsg
        # When topic is modified
        self.irc.client.topicUpdated = self.topic_updated

    def send_message(self, channel, multiline_message=""):
        """
        Sends a message on specified channel, cutting each line in a new message
        """
        if isinstance(multiline_message, list):
            multiline_message = "\n".join(multiline_message)
        for message in multiline_message.splitlines():
            self.irc.client.msg(channel, message)

    def topic_updated(self, user, channel, newTopic):
        """
        Topic has been modified, or bot is coming in the channel.
        If the bot is coming in, we can start its timer's loop
        """
        self.topic = newTopic
        # Can I start the loop ?
        if not self.loop_started:
            self.loop_started = True
            self.loop.start(config.timer)

    def on_privmsg(self, user, channel, msg):
        """
        Wantzel can understand a lot of commands. Commands followed by a (*)
        are accessible only to moderators:
        - help
            Returns a message about how to use the bot.
            If a command is passed after help, the message explains how to use
            the command.
            Bot answers in private in order not to spam channel
        - rp(acp) <url>
            Add an article in the database with a specific flag
        - status <url>
            Retrieve some informations about an article in the database
        - stats
            Show some statistics about the RP
        - kill (*)
            Kill an article by giving it a score of -100
        - admin list (*)
            List rights in private
        - admin add (*)
            Add one or more new moderator to list
        - admin del (*)
            Delete one or more moderator from list
        - admin timer
            Relaunch a timer
        """
        # Cleaning user name
        user = re.search("([^!]*)!", user).group(1)
        Log.debug("Message received: %s %s %s" % (user, channel, msg))
        # Never answer to botself
        if user!=config.nickname:
            # If it's a query, bot should answer to the user as the channel
            if "#" not in channel:
                channel = user
            # Help command, specific
            if "wantzel" in msg and ("help" in msg or "aide" in msg):
                self.help(user, channel, msg)
            # Find known command
            command = re.search("[!~](rp[acp]*|status|kill|help|stats|admin)", msg)
            Log.debug("Command: %s" % command)
            if command:
                Log.debug("group(0): %s" % command.group(0))
                command = command.group(1)
                Log.debug("Command: %s" % command)
                if command.startswith("rp"):
                    Log.debug("Calling self.rp")
                    self.send_message(channel, self.rp.rp(command, user, channel, msg))
                if command.startswith("status"):
                    Log.debug("Calling self.status")
                    self.send_message(channel, self.rp.status(user, msg))
                elif command == "help":
                    Log.debug("Calling self.help")
                    self.help(user, channel, msg)
                elif command == "kill":
                    Log.debug("Calling self.kill")
                    self.send_message(channel, self.rp.kill(user, msg))
                elif command == "stats":
                    Log.debug("Calling self.stats")
                    self.send_message(channel, self.rp.stats())
                elif command == "admin":
                    Log.debug("Calling self.admin")
                    result = self.admin.admin(user, msg)
                    if result=="REACTOR":
                        try:
                            # Re-creating timer
                            self.loop = task.LoopingCall(self.timer)
                            self.loop.start(config.timer)
                        except Exception:
                            pass
                    elif result=="TEST_REACTOR":
                        self.test_timer = channel
                        self.send_message(channel, "Test du timer en cours...")
                    else:
                        self.send_message(channel, result)

    def help(self, user, channel, msg):
        """
        Show global help.
        If a known command is behind the ~!help command, an adequate message is
        returned.
        """
        Log.debug("help command")
        # Searching for a command after help keyword
        command = re.search("[!~]help (help|rp|status|stats|kill|admin)", msg)
        if command:
            command = command.group(1)
            print(command)
            if command=="help":
                self.send_message(channel, helpers.help_help % user)
            elif command=="rp":
                self.send_message(channel, helpers.help_rp % user)
            elif command=="status":
                self.send_message(channel, helpers.help_status % user)
            elif command=="stats":
                self.send_message(channel, helpers.help_stats % user)
            elif command=="kill":
                self.send_message(channel, helpers.help_kill % user)
            elif command=="admin":
                self.send_message(channel, helpers.help_admin % user)
            else:
                self.send_message(channel, helpers.help_unknown % user)
        else:
            self.send_message(channel, helpers.help % user)

if __name__ == '__main__':
    Wantzel()
    reactor.run()
