#encoding: utf-8
"""
Messages destinés à l'aide
"""

help = """
%s Mes commandes sont : ~help ~rp(cpa) ~status ~kill ~stats et ~admin.
    Pour plus d'informations, voir ici: https://wiki.laquadrature.net/Wantzel
    Pour obtenir de l'aide sur une commande en particulier, il suffit de taper ~help <commande>
"""

help_help = """
Bravo %s!
Tu viens d'entrer dans le monde récursif où l'aide sert à expliciter l'aide.
"""

help_rp = """
%s, cette commande sert à ajouter un article à la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
L'utilisation se fait sous la forme: ~rp(cpa) <url de l'article à ajouter>
"""

help_status = """
%s, cette commande sert à retrouver les informations concernant un article ajouté à la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
L'utilisation se fait sous la forme: ~status <url de l'article>
"""

help_stats = """
%s, cette commande permet de fournir quelques statistiques sur la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
Les statistiques sont calculées sur des notes supérieurs ou égales à 0, 3, et 4. Et sur les 1, 3, 7, et 15 derniers jours.
""" 

help_kill = """
*Attention %s* seuls les vrais rp-jedis ont accès à cette commande <3
    Fixe la note de l'article donné en paramètre à -100.
    Utile en cas d'erreur ou pour s'assurer que l'article ne sera pas publié dans la RP
    Utilisation: ~kill <url de l'article>
"""

help_admin = """
*Attention %s* seuls les vrais rp-jedis ont accès à cette commande <3
    Permet de gérer la liste des utilisateurs ayant un accès privilégié. Il n'y a qu'un seul niveau de privilège.
    Utilisation:
        ~admin list                 Fournit la liste des utilisateurs privilégiés
        ~admin add user[, user]>    Ajoute un ou plusieurs utilisateurs à la liste
        ~admin del user[, user]     Supprime un ou plusieurs utilisateurs de la liste
        ~admin testtimer            Teste le timer
        ~admin timer                Relance un timer pour gérer le topic et les tweets
"""

help_unknown = """
Désolé %s, je ne connais pas cette commande.
"""




