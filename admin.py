#encoding: utf-8
"""
Monitoring methods.
"""

import re
import sqlite3

import config
from logs import Log
from utils import is_moderator

class Admin():
    def admin(self, user, msg):
        """
        Manage moderation.
        A sub-command should be behind the !~admin command.
        """
        Log.debug("admin command")
        # Searching for a command after admin keyword
        command = re.search("[~!]admin (list|add|del|timer|testtimer)", msg)
        if command:
            command = command.group(1)
            if command == "list":
                return self.admin_list(user)
            elif command == "add":
                return self.admin_add(user, msg)
            elif command == "del":
                return self.admin_del(user, msg)
            elif command == "timer":
                return self.admin_timer(user)
            elif command == "testtimer":
                return self.admin_testtimer(user)

    def admin_list(self, user):
        """
        List actual moderators.
        """
        Log.debug("admin_list command")
        if is_moderator(user):
            connection = sqlite3.connect(config.sqlite_db)
            names = []
            for row in connection.execute("SELECT name FROM moderator"):
                names.append(row[0].encode("utf-8"))
            return "Liste des modérateurs actuels: %s" % ", ".join(sorted(names))
        else:
            return "Désolé, il ne semble pas que vous ayez les droits pour cette commande."

    def admin_add(self, user, msg):
        """
        Add some new moderators if not existing yet.
        """
        Log.debug("admin_add command")
        if is_moderator(user):
            try:
                names = []
                connection = sqlite3.connect(config.sqlite_db)
                result = re.search("[!~]admin add (([^,]+, ?)+)?(.*)", msg)
                if result.group(1):
                    names = [name.strip() for name in result.group(1).split(",") if name.strip() != ""]
                names.append(result.group(3))
                # Do not add actual moderators
                moderators = []
                for row in connection.execute("SELECT name FROM moderator"):
                    moderators.append(row[0].encode("utf-8"))
                names = list(set([name for name in names if name not in moderators]))
                if names:
                    # Converting set in list of tuples
                    values = [(name,) for name in names]
                    connection.executemany("INSERT INTO moderator (name) VALUES (?)", values)
                    connection.commit()
                    return "%s a(ont) été ajouté(s) à la liste des modérateurs" % ", ".join(names)
                else:
                    return "Ces utilisateurs sont déjà modérateurs"
            except Exception:
                return ""
        else:
            return "Désolé, il ne semble pas que vous ayez les droits pour cette commande."

    def admin_del(self, user, msg):
        """
        Delete a moderator from list.
        """
        Log.debug("admin_del command")
        if is_moderator(user):
            try:
                names = []
                result = re.search("[!~]admin del (([^,]+, ?)+)?(.*)", msg)
                if result.group(1):
                    names = [name.strip() for name in result.group(1).split(",") if name.strip() != ""]
                names.append(result.group(3))
                names = list(set(names))
                Log.debug(names)
                connection = sqlite3.connect(config.sqlite_db)
                for name in names:
                    connection.execute("DELETE FROM moderator WHERE name=?", (name, ))
                connection.commit()
                return "%s a(ont) été retiré(s) de la liste des modérateurs." % ", ".join(names)
            except Exception:
                return ""
        else:
            return "Désolé, il ne semble pas que vous ayez les droits pour cette commande."

    def admin_timer(self, user):
        """
        Relaunch a timer.
        """
        Log.debug("admin_timer command")
        if is_moderator(user):
            return "REACTOR"
        else:
            return "Désolé, il ne semble pas que vous ayez les droits pour cette commande."

    def admin_testtimer(self, user):
        """
        Test if timer is still alive.
        """
        Log.debug("admin_testtimer command")
        if is_moderator(user):
            return "TEST_REACTOR"
        else:
            return "Désolé, il ne semble pas que vous ayez les droits pour cette commande."
